package by.shag.golatina.service;

import by.shag.golatina.api.dto.UserDto;
import by.shag.golatina.jpa.model.User;
import by.shag.golatina.jpa.repository.UserRepository;
import by.shag.golatina.mapping.UserDtoMapper;

import java.util.List;
import java.util.stream.Collectors;

public class UserService {

    private UserRepository userRepository;

    private UserDtoMapper mapper;

    public UserDto save(UserDto userDto) {
        User user = mapper.map(userDto);
        User saved = userRepository.save(user);
        return mapper.map(saved);
    }

    public UserDto findByID(Integer id) {
        User user = userRepository.findByID(id);
        return mapper.map(user);
    }

    public List<UserDto> findAll() {
        return userRepository.findAll().stream()
                .map(user -> mapper.map(user))
                .collect(Collectors.toList());
    }

    public void deleteByID(Integer id) {
        userRepository.deleteByID(id);
    }

    public UserDto update(UserDto userDto) {
        User user = mapper.map(userDto);
        User saved = userRepository.update(user);
        return mapper.map(saved);
    }

}
