package by.shag.golatina.jpa.model;

public class User {

    private Integer id;
    private String name;
    private String lastName;
    private String login;
    private int age;

    public User(Integer id, String name, String lastName, String login, int age) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.login = login;
        this.age = age;
    }

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
