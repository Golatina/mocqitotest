package by.shag.golatina.jpa.repository;

import by.shag.golatina.jpa.model.User;

import java.util.List;

public class UserRepository {

    public User save(User user){
        throw new UnsupportedOperationException();
    }

    public User findByID(Integer id){
        throw new UnsupportedOperationException();
    }

    public List<User> findAll(){
        throw new UnsupportedOperationException();
    }

    public void deleteByID(Integer id){
        throw new UnsupportedOperationException();
    }

    public User update(User user){
        throw new UnsupportedOperationException();
    }
}
