package by.shag.golatina.old;

import by.shag.golatina.old.quest2.Jupiter;
import by.shag.golatina.old.quest2.Planet;
import by.shag.golatina.old.quest2.Venus;
import by.shag.golatina.old.quest4.Body;
import by.shag.golatina.old.quest4.Frankenstein;
import by.shag.golatina.old.quest4.Head;
import by.shag.golatina.old.quest4.Limbs;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RunnerOld {

    public static void quest1(String[] strings) {
        int iWhile = 0;
        while (iWhile < strings.length) {
            System.out.print(strings[iWhile] + " ");
            iWhile++;
        }
        System.out.println();
        for (int i = strings.length - 1; i >= 0; i--) {
            System.out.print(strings[i] + " ");
        }
        System.out.println();
        for (String str : strings) {
            System.out.print(str.charAt(0));
        }
        System.out.println();
        for (String str : strings) {
            System.out.print(str.charAt(str.length() - 1));
        }
        System.out.println();
    }

    public static void quest3(Object o) {
        if (o instanceof Planet) {
            throw new IllegalArgumentException("не кидайте в меня планетами");
        } else System.out.println("написать в консоль, что все ок");
    }

    public static void quest5(List<String> stringList) {
        Iterator<String> iter = stringList.iterator();
        while (iter.hasNext()) {
            iter.next();
            if (iter.hasNext()) {
                iter.next();
                if (iter.hasNext()) {
                    System.out.println(iter.next());
                }
            }
        }
    }

    public static Map<String, Integer> quest6(String string) {
        return Arrays.stream(string.replaceAll(",", "").split(" "))
                .collect(Collectors.groupingBy(Function.identity(), TreeMap::new, Collectors.collectingAndThen(Collectors.counting(), Long::intValue)));
    }

    public static void main(String[] args) {
        String[] strings = new String[]{"Иногда", "цвет", "марсианского", "неба", "приобретает", "фиолетовый", "оттенок", "в", "результате", "рассеяния", "света", "на", "микрочастицах", "водяного", "льда", "в", "облаках"};
        quest1(strings);

        Jupiter jupiter = new Jupiter();
        Venus venus = new Venus();
        jupiter.talk();
        jupiter.talk(strings);
        venus.talk();

//        quest3(venus);
        quest3(strings);

        Body[] bodies = {Body.MAN, Body.DRAGON, Body.WOMAN};
        Head[] heads = {Head.BULL, Head.CAT, Head.CROCODILE, Head.HIPPOPOTAMUS};
        Limbs[] limbs = {Limbs.BIRD, Limbs.GOAT, Limbs.HUMAN, Limbs.SPIDER, Limbs.OCTOPUS};

        Frankenstein frankenstein1 = new Frankenstein(bodies[(int) (Math.random() * bodies.length)],
                heads[(int) (Math.random() * heads.length)],
                limbs[(int) (Math.random() * limbs.length)]);
        Frankenstein frankenstein2 = new Frankenstein(bodies[(int) (Math.random() * bodies.length)],
                heads[(int) (Math.random() * heads.length)],
                limbs[(int) (Math.random() * limbs.length)]);
        System.out.println(frankenstein1 + "\n" + frankenstein2);

        List<String> stringList = new ArrayList<>();
        Collections.addAll(stringList, strings);
        quest5(stringList);

        String string = "Земля по отношению к Марсу является внутренней планетой, так же, как Венера для Земли. " +
                "Соответственно, с Марса Земля наблюдается как утренняя или вечерняя звезда, восходящая перед рассветом или видимая на вечернем небе после захода Солнца.";
        System.out.println(quest6(string));
    }
}
