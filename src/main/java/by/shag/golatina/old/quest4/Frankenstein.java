package by.shag.golatina.old.quest4;

public class Frankenstein {

    private Body body;
    private Head head;
    private Limbs limbs;

    public Frankenstein(Body body, Head head, Limbs limbs) {
        this.body = body;
        this.head = head;
        this.limbs = limbs;
    }

    @Override
    public String toString() {
        return "Frankenstein{" +
                "body=" + body +
                ", head=" + head +
                ", limbs=" + limbs +
                '}';
    }
}
