package by.shag.golatina.mapping;

import by.shag.golatina.api.dto.UserDto;
import by.shag.golatina.jpa.model.User;

public class UserDtoMapper {

    public User map(UserDto userDto) {
        User user = new User();
        user.setAge(userDto.getAge());
        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setLastName(userDto.getLastName());
        user.setLogin(userDto.getLogin() == null ? userDto.getEmail() : userDto.getLogin());
        return user;
    }

    public UserDto map(User user) {
        UserDto userDto = new UserDto();
        userDto.setAge(user.getAge());
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setLastName(user.getLastName());
        userDto.setLogin(user.getLogin());
        return userDto;
    }
}
