package by.shag.golatina.mapping;

import by.shag.golatina.api.dto.UserDto;
import by.shag.golatina.jpa.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserDtoMapperForCheck {

    private static UserDto userDto;

    private final Integer ID = 1;
    private final String NAME = "Name";
    private final String LASTNAME = "LastName";
    private final String LOGIN = "ZXC_ASD";
    private final String LOGIN_NULL = null;
    private final String EMAIL = "zxc@asd.com";
    private final int AGE = 12;

    private UserDtoMapper userDtoMapper;

    @BeforeEach
    void beforeEach() {
        userDtoMapper = new UserDtoMapper();
    }

    @Test
    void mapUserDtoToUserWithNullLogin() {
        userDto = new UserDto(ID, NAME, LASTNAME, LOGIN_NULL, EMAIL, AGE);
        User result = userDtoMapper.map(userDto);

        assertEquals(result.getAge(), AGE);
        assertEquals(result.getLogin(), EMAIL);
        assertEquals(result.getId(), ID);
        assertEquals(result.getLastName(), LASTNAME);
        assertEquals(result.getName(), NAME);
    }

    @Test
    void mapUserDtoToUserLoginNotNull() {
        userDto = new UserDto(ID, NAME, LASTNAME, LOGIN, EMAIL, AGE);
        User result = userDtoMapper.map(userDto);

        assertEquals(result.getAge(), AGE);
        assertEquals(result.getLogin(), LOGIN);
        assertEquals(result.getId(), ID);
        assertEquals(result.getLastName(), LASTNAME);
        assertEquals(result.getName(), NAME);
    }

    @Test
    void mapUserToUserDto() {
        User user = new User(ID, NAME, LASTNAME, LOGIN, AGE);
        UserDto result = userDtoMapper.map(user);

        assertEquals(result.getAge(), AGE);
        assertEquals(result.getLogin(), LOGIN);
        assertEquals(result.getId(), ID);
        assertEquals(result.getLastName(), LASTNAME);
        assertEquals(result.getName(), NAME);
        assertNull(result.getEmail());
    }

}
