package by.shag.golatina.mapping;

import by.shag.golatina.api.dto.UserDto;
import by.shag.golatina.jpa.model.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserDtoMapperTest {

    private final Integer ID = 1;
    private final String NAME = "Name";
    private final String LASTNAME = "LastName";
    private final String LOGIN = "ZXC_ASD";
    private final String LOGIN_NULL = null;
    private final String EMAIL = "zxc@asd.com";
    private final int AGE = 12;

    private UserDtoMapper userDtoMapper = new UserDtoMapper();

    @Test
    void mapActorDtoToActorWithNullLogin(){
        UserDto userDto = new UserDto();
        userDto.setLogin(LOGIN_NULL);
        userDto.setName(NAME);
        userDto.setId(ID);
        userDto.setAge(AGE);
        userDto.setLastName(LASTNAME);
        userDto.setEmail(EMAIL);

        User result = userDtoMapper.map(userDto);

        assertEquals(result.getAge(),AGE);
        assertEquals(result.getLogin(),EMAIL);
        assertEquals(result.getId(),ID);
        assertEquals(result.getLastName(),LASTNAME);
        assertEquals(result.getName(),NAME);
    }

    @Test
    void mapActorDtoToActorLoginNotNull(){
        UserDto userDto = new UserDto();
        userDto.setLogin(LOGIN);
        userDto.setName(NAME);
        userDto.setId(ID);
        userDto.setAge(AGE);
        userDto.setLastName(LASTNAME);
        userDto.setEmail(EMAIL);

        User result = userDtoMapper.map(userDto);

        assertEquals(result.getAge(),AGE);
        assertEquals(result.getLogin(),LOGIN);
        assertEquals(result.getId(),ID);
        assertEquals(result.getLastName(),LASTNAME);
        assertEquals(result.getName(),NAME);
    }

    @Test
    void mapActorToActorDto(){
        User user = new User();
        user.setLogin(LOGIN);
        user.setName(NAME);
        user.setId(ID);
        user.setAge(AGE);
        user.setLastName(LASTNAME);

        UserDto result = userDtoMapper.map(user);

        assertEquals(result.getAge(),AGE);
        assertEquals(result.getLogin(),LOGIN);
        assertEquals(result.getId(),ID);
        assertEquals(result.getLastName(),LASTNAME);
        assertEquals(result.getName(),NAME);
        assertNull(result.getEmail());
    }
}