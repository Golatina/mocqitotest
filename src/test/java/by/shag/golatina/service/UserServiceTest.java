package by.shag.golatina.service;

import by.shag.golatina.api.dto.UserDto;
import by.shag.golatina.jpa.model.User;
import by.shag.golatina.jpa.repository.UserRepository;
import by.shag.golatina.mapping.UserDtoMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class UserServiceTest {

    private static User user;
    private static User userAfterOperation;
    private static UserDto userDto;
    private static UserDto userDtoAfterOperation;
    private static List<User> userList;
    private static List<UserDto> userDtoList;
    private static Integer quasi;
    @Mock
    private UserRepository userRepository;
    @Mock
    private UserDtoMapper mapper;
    @InjectMocks
    private UserService userService;

    @BeforeEach
    void beforeEach(){
        user = new User(1, "Test-Name1", "Test-lastName1", "Test-login1", 10);
        userAfterOperation = new User(2, "Test-Name2", "Test-lastName2", "Test-login2", 20);
        userDto = new UserDto(10, "Test-NameDTO1", "Test-lastNameDTO1", "Test-loginDTO1", "Test-emailDTO1", 100);
        userDtoAfterOperation = new UserDto(20, "Test-NameDTO2", "Test-lastNameDTO2", "Test-loginDTO2", "Test-emailDTO2", 200);
        userList = Arrays.asList(user, user, user);
        userDtoList = Arrays.asList(userDto, userDto, userDto);
        quasi = user.getId();
    }

    @AfterEach
    void teardown() {
        verifyNoMoreInteractions(mapper, userRepository);
    }

    @Test
    void save() {
        when(mapper.map(any(UserDto.class))).thenReturn(user);
        when(userRepository.save(any(User.class))).thenReturn(userAfterOperation);
        when(mapper.map(any(User.class))).thenReturn(userDtoAfterOperation);

        UserDto result = userService.save(userDto);

        assertEquals(result, userDtoAfterOperation);
        verify(mapper).map(userDto);
        verify(userRepository).save(user);
        verify(mapper).map(userAfterOperation);
    }

    @Test
    void findByID() {
        when(userRepository.findByID(anyInt())).thenReturn(user);
        when(mapper.map(any(User.class))).thenReturn(userDto);

        UserDto result = userService.findByID(quasi);

        assertEquals(result, userDto);
        verify(userRepository).findByID(quasi);
        verify(mapper).map(user);
    }

    @Test
    void findAll() {
        when(userRepository.findAll()).thenReturn(userList);
        when(mapper.map(any(User.class))).thenReturn(userDto);

        List<UserDto> result = userService.findAll();

        assertArrayEquals(userDtoList.toArray(), result.toArray());
        verify(mapper, times(3)).map(user);
        verify(userRepository).findAll();
    }

    @Test
    void deleteByID() {
        userService.deleteByID(quasi);

        verify(userRepository).deleteByID(quasi);
    }

    @Test
    void update() {
        when(mapper.map(any(UserDto.class))).thenReturn(user);
        when(userRepository.update(any(User.class))).thenReturn(userAfterOperation);
        when(mapper.map(any(User.class))).thenReturn(userDtoAfterOperation);

        UserDto result = userService.update(userDto);

        assertEquals(result, userDtoAfterOperation);
        verify(mapper).map(userDto);
        verify(userRepository).update(user);
        verify(mapper).map(userAfterOperation);
    }

}